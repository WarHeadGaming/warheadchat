package com.warhead.warheadchat;

import com.warhead.warcraftutils.WarcraftUtils;
import com.warhead.warcraftutils.settings.ConfigFile;
import com.warhead.warcraftutils.util.ChatFormat;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.spout.api.entity.Player;

/**
 *
 * @author Koo
 */
public class ChatChannels {
    private static WarheadChat wc;
    private static final Logger log = Logger.getLogger("Minecraft");
    private static WarcraftUtils wu;
    private static ConfigFile cf;
    private static ChatFormat help;
    String gap = "   ";
    
    public static HashMap<String,String> playerChannels = new HashMap<String,String>();
    
    public void ChatChannels(){
        wc = new WarheadChat();
        wu = WarcraftUtils.getInstance();
        help = wc.getHelp();
    }
    
    /**
     * Returns TRUE if player is in a channel. Mostly used for debugging.
     * 
     * @param player
     * @return Boolean
     */
    public static boolean inChannel(Player player){
        boolean a = false;
        if(playerChannels.containsKey(player.getName())){
            a = true;
        }
        return a;
    }
    
    /**
     * Sets the players current channel.
     * 
     * @param player
     * @param channel 
     */
    public static void setPlayerChannel(Player player, String channel){
        if(channelExists(channel)){
            playerChannels.put(player.getName(), channel);
            player.sendMessage("Now chatting in ", channel, "!");
        } else {
            player.sendMessage(wc.pluginName, "That channel doesn't exist!");
        }
    }
    
    /**
     * Adds a player as owner to given channel.
     * 
     * @param player
     * @param channel
     */
    public void addOwner(Player player, String channel){
        cf = new ConfigFile("plugins/WarheadChat/Channels/",channel);
        String owners = cf.getString("Owners");
        owners = owners + "," + player.getName();
        if(owners.startsWith(",")){
            owners = owners.replaceFirst(",", "");
        }
        cf.setString("Owners", owners);
    }
    
    /**
     * Returns all channels avaliable to a player.
     * 
     * getAllChannels will return a completed list of channels to a player who is
     * either an owner of or member of a channel. IF the channel contains '@ALL'
     * in members list, it will return to ALL players.
     * 
     * @param player
     */
    public void getAllChannels(Player player){
        ArrayList<String> list = getChannels();
        player.sendMessage("-----------------------");
        player.sendMessage("All avaliable channels:");
        for(int x = 0; x < list.size(); x++){
            if(isMember(player, list.get(x)) || isOwner(player, list.get(x)) || getMembers(list.get(x))[0].equals("@ALL"))
                player.sendMessage(gap, list.get(x));
        }
        player.sendMessage("-----------------------");
    }
    
    /**
     * Adds a member to a given channel. IF channels members is '@ALL', it will 
     * do nothing.
     * 
     * @param player
     * @param channel 
     */
    public void addMember(Player player, String channel){
        cf = new ConfigFile("plugins/WarheadChat/Channels/",channel);
        if(cf.getString("Members").equals("@ALL")){
            //DO NOTHING!
        } else {
            String members = cf.getString("Members");
            members = members + "," + player.getName();
            if(members.startsWith(",")){
                members = members.replaceFirst(",", "");
            }
            cf.setString("Members", members);
        }
    }
    
    /**
     * Return TRUE if player is a member of given channel. IF channel's members 
     * is '@ALL' then it will return true.
     * 
     * @param player
     * @param channel
     * @return Boolean
     */
    public static boolean isMember(Player player, String channel){
        String[] members = getMembers(channel);
        boolean answer = false;
        if(members[0].equals("@ALL")){
            answer = true;
        } else {
            if(members.length != 0){
                for(int x = 1; x <= members.length; x++){
                    if(members[x-1].equals(player.getName())){
                        answer = true;
                        break;
                    }
                }
            }
        }
        return answer;
    }
    
    /**
     * Returns TRUE if player is owner of given channel.
     * 
     * @param player
     * @param channel
     * @return Boolean
     */
    public static boolean isOwner(Player player, String channel){
        String[] owners = getOwners(channel);
        boolean answer = false;
        if(owners.length != 0){
            for(int x = 1; x <= owners.length; x++){
                if(owners[x-1].equals(player.getName())){
                    answer = true;
                    break;
                }
            }
        }
        return answer;    
    }
    
    /**
     * Returns a String Array of all player names in a given channel.
     * 
     * @param channel
     * @return String[]
     */
    public static String[] getMembers(String channel){
        String[] members = null;
        cf = new ConfigFile("plugins/WarheadChat/Channels/",channel);
        String memberString = cf.getString("Members");
        members = memberString.split(",");
        return members;
    }
    
    /**
     * Returns alias for given channel.
     * 
     * @param channel
     * @return String
     */
    public static String getChannelAlias(String channel){
        String alias;
        
        cf = new ConfigFile("plugins/WarheadChat/Channels/",channel);
        
        alias = cf.getString("Alias");
        
        return alias;
    }
    
    /**
     * Removes a given channel.
     * 
     * Removes a channel from the Channels folder. IF player is not owner,
     * it will not remove channel. If player is given warhead.chat.admin rights,
     * it will be removed.
     * 
     * @param player
     * @param channel 
     */
    public void removeChannel(Player player, String channel){
        String path = "plugins/WarheadChat/Channels/";
        File file = new File(path + channel);
        if(!player.hasPermission("warhead.chat.admin")){
            if(this.channelExists(channel)){
                if(this.isOwner(player, channel)){
                    boolean worked = file.delete();
                    if(!worked){
                        log.severe(wc.pluginName + " Could not delete channel!");
                        player.sendMessage("Something went wrong! Channels NOT removed!");
                    } else {
                        log.info(wc.pluginName + " Channel " + channel + " has been removed.");
                        player.sendMessage("Channels removed!");
                    }
                } else {
                    player.sendMessage("You are not an owner of that channel!");
                }
            } else {
                player.sendMessage("That channel does not exist!");

            }
        } else {
            file.delete();
            player.sendMessage("You are admin! Channel deleted.");
        }
    }
    
        /**
     * Removes a given channel.
     * 
     * Removes a channel from the Channels folder.
     * 
     * @param player
     * @param channel 
     */
    public void removeChannel(String channel){
        String path = "plugins/WarheadChat/Channels/";
        File file = new File(path + channel);
            if(this.channelExists(channel)){
                boolean worked = file.delete();
                if(!worked){
                    log.severe(wc.pluginName + " Could not delete channel!");
                } else {
                    log.info(wc.pluginName + " Channel " + channel + " has been removed.");
                }
            } else {
                log.info("That channel does not exist!");

            }
    }

    /**
     * Returns String Array of all Owners in a given channel.
     * 
     * @param channel
     * @return String[]
     */
    public static String[] getOwners(String channel){
        String[] owners = null;
        cf = new ConfigFile("plugins/WarheadChat/Channels/",channel);
        String ownerString = cf.getString("Owners");
        owners = ownerString.split(",");
        return owners;
    }
    
    /**
     * Returns name of given player's current channel.
     * 
     * @param player
     * @return String
     */
    public static String getPlayersChannel(Player player){
        String answer = null;
        if(playerChannels.containsKey(player.getName())){
            answer = playerChannels.get(player.getName());
        }
        return answer;
    }
    
    /**
     * Creates a channel w/ auto alias.
     * 
     * Created a channel with the given name. Auto sets values for an alias with
     * the first three letters of the channel name. IF the channels exists, nothing
     * nothing happens. Makes the given player an owner as soon as file is created.
     * 
     * @param name
     * @param player 
     */
    public static void createChannel(String name, Player player) {
        if(!channelExists(name)){
            cf = new ConfigFile("plugins/WarheadChat/Channels/",name);
            if (!cf.keyExists("DisplayName")) {
                cf.setString("DisplayName", name.substring(0, 3));
            }
            if (!cf.keyExists("Alias")) {
                cf.setString("Alias", name.substring(0, 2));
            }
            if (!cf.keyExists("OwnerGroup")) {
                cf.setString("OwnerGroup", "");
            }
            if (!cf.keyExists("MemberGroup")) {
                cf.setString("MemberGroup", "");
            }
            if (!cf.keyExists("Owners")) {
                cf.setString("Owners", player.getName());
            }
            if (!cf.keyExists("Members")) {
                cf.setString("Members", "");
            }
        } else {
            player.sendMessage(wc.pluginName ," Channel already exists!");
        }
        createFinish(player, name);
    }
    
    private static void createFinish(Player player, String channel){
        if(channelExists(channel)){
            log.log(Level.INFO, "{0}{1} created channel {2}", new Object[]{wc.pluginName, player.getName(), channel});
            player.sendMessage("Created channel!");
        } else{
            player.sendMessage("Something went wrong!");
        }
    }
    
    /**
     * Creates a channel.
     * 
     * Creates a channel with an alias given. IF the channels exists, nothing
     * nothing happens. Makes the given player an owner as soon as file is created.
     * 
     * @param name
     * @param alias
     * @param player 
     */
    public void createChannel(String name, String alias, Player player){
        if(!channelExists(name)){
            cf = new ConfigFile("plugins/WarheadChat/Channels",name);
            if (!cf.keyExists("DisplayName")) {
                cf.setString("DisplayName", name.substring(0, 3));
            }
            if (!cf.keyExists("Alias")) {
                cf.setString("Alias", name.substring(0, 2));
            }
            if (!cf.keyExists("OwnerGroup")) {
                cf.setString("OwnerGroup", "");
            }
            if (!cf.keyExists("MemberGroup")) {
                cf.setString("MemberGroup", "");
            }
            if (!cf.keyExists("Owners")) {
                cf.setString("Owners", player.getName());
            }
            if (!cf.keyExists("Members")) {
                cf.setString("Members", "");
            }
        }
    }
    
    /**
     * Returns Boolean if the given channel exists.
     * 
     * @param channel
     * @return Boolean
     */
    public static boolean channelExists(String channel){
        ArrayList<String> list = getChannels();
        if(list.contains(channel)){
            return true;
        } else {
            return false;
        }
    }
    
    /**
     * Returns ArrayList of ALL channels.
     * 
     * @return ArrayList<String>
     */
    private static ArrayList<String> getChannels(){
        ArrayList<String> list = new ArrayList<String>();
        String path = "plugins/WarheadChat/Channels";

        String file;
        File folder = new File(path);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {

            if (listOfFiles[i].isFile()) {
                file = listOfFiles[i].getName();
                list.add(file);
            }
        }
        return list;
    }
    

}
