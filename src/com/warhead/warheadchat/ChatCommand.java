/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.warhead.warheadchat;

import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.command.annotated.NestedCommand;
import org.spout.api.exception.CommandException;

/**
 *
 * @author Douglas
 */
public class ChatCommand {
    private final WarheadChat plugin;
    
    public ChatCommand(WarheadChat instance){
        plugin = instance;
    }
    
    @Command(aliases = {"channels", "chnl", "channel"}, usage = "", desc = "Access Channels commands", min = 1, max = 1)
    @NestedCommand(ChatCommands.class)
    public void channels(CommandContext cc, CommandSource cs) throws CommandException{
        
    }
}
