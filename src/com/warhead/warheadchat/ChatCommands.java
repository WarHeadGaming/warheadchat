package com.warhead.warheadchat;

import org.spout.api.command.CommandContext;
import org.spout.api.command.CommandSource;
import org.spout.api.command.annotated.Command;
import org.spout.api.entity.Player;
import org.spout.api.exception.CommandException;

/**
 *
 * @author Koo
 */
public class ChatCommands{
    String gap = "   ";
    private final WarheadChat plugin;
    private static ChatChannels chatChannels;
    
    public ChatCommands(WarheadChat instance) {
		plugin = instance;
                chatChannels = plugin.getChatChannels();
    }
    
    @Command(aliases = {"switch","swch","Change"}, usage = "", desc = "Change Channels", min = 1, max = 2)
//    @CommandPermissions("WarheadChat.switch")
    public void channelSwitch(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player)cs;
        if(cs instanceof Player){
            if(cc.length() < 2){
                player.sendMessage(cc.getString(0));
                chatChannels.setPlayerChannel(player, cc.getString(0));
            }
        }
    }
    
    @Command(aliases = {"create","crt","make","mk"}, usage = "", desc = "Make new Channel", min = 1, max = 2)
    public void channelCreate(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player)cs;
        if(cs instanceof Player){
            if(cc.getString(0).length() < 3){
                player.sendMessage("Channel name is to short! ");
            } else {
                if(cc.length() == 2){
                    chatChannels.createChannel(cc.getString(0),cc.getString(1),player);
                } else {
                    if(cc.length() == 1){
                        chatChannels.createChannel(cc.getString(0), player);
                    }
                }
            }
        }
    }
    
    @Command(aliases = {"remove","rmv","delete"}, usage = "", desc = "Remove a channel", min = 1, max = 1)
    public void channelRemove(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player)cs;
        if(cs instanceof Player){
            if(cc.length() == 1){
                chatChannels.removeChannel(player, cc.getString(0));
            }
        }
    }
    
    @Command(aliases = {"avaliable","list","ls"}, usage = "", desc = "Get all channels", min = 0, max = 0)
    public void listChannels(CommandContext cc, CommandSource cs) throws CommandException{
        Player player = (Player)cs;
        if(cs instanceof Player){
            chatChannels.getAllChannels(player);
        }
    }
    
    @Command(aliases = {"help"}, usage = "", desc = "Get all Channel commands", min = 0, max = 0)
    public void getHelp(CommandContext cc, CommandSource cs) throws CommandException{
        plugin.sendHelp((Player) cs);
    }
}
