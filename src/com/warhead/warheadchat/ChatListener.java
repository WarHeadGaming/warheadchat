package com.warhead.warheadchat;

import java.util.logging.Logger;
import org.spout.api.Server;
import org.spout.api.Spout;
import org.spout.api.chat.ChatArguments;
import org.spout.api.entity.Player;
import org.spout.api.event.EventHandler;
import org.spout.api.event.Listener;
import org.spout.api.event.Order;
import org.spout.api.event.player.PlayerChatEvent;
import org.spout.api.event.player.PlayerJoinEvent;


/**
 *
 * @author Koo
 */
public class ChatListener implements Listener {
    private static WarheadChat plugin;
    private static ChatChannels cc;
    private static final Logger log = Logger.getLogger("Minecraft");
    
    
    public void ChatListener() {
        plugin = WarheadChat.getInstance();
        cc = plugin.getChatChannels();
    }
    
    @EventHandler
    public void ChatEvent(PlayerChatEvent event){
        ChatArguments prefix = new ChatArguments();
        prefix.append("(" + ChatChannels.getChannelAlias(ChatChannels.getPlayersChannel(event.getPlayer())) + ") " + event.getPlayer().getDisplayName() + ": ");
        ChatArguments message = event.getMessage();
        String a = message.asString();
        prefix.append(a);
        Player[] list = ((Server) Spout.getEngine()).getOnlinePlayers();
        
        String channel = ChatChannels.getPlayersChannel(event.getPlayer());
        event.setCancelled(true);
        for(int x = 0; x < list.length; x++){
            Player player = list[x];
            if(cc.isMember(player, channel) || cc.isOwner(player, channel)){
                player.sendMessage(prefix);
            }
        }
    }
    
    @EventHandler(order = Order.EARLIEST)
    public void onPlayerJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();
            cc.setPlayerChannel(player, "Default");
        player.sendMessage("Your current Channel: "+ChatChannels.getPlayersChannel(player));
    }
}