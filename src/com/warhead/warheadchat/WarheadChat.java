package com.warhead.warheadchat;

import com.warhead.warcraftutils.WarcraftUtils;
import com.warhead.warcraftutils.settings.ConfigFile;
import com.warhead.warcraftutils.util.ChatFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import org.spout.api.Spout;
import org.spout.api.chat.style.ChatStyle;
import org.spout.api.command.CommandRegistrationsFactory;
import org.spout.api.command.annotated.AnnotatedCommandRegistrationFactory;
import org.spout.api.command.annotated.SimpleAnnotatedCommandExecutorFactory;
import org.spout.api.command.annotated.SimpleInjector;
import org.spout.api.entity.Player;
import org.spout.api.plugin.CommonPlugin;

/**
 * Main class
 *
 * @author Koo
 */
public class WarheadChat extends CommonPlugin{
    
    private static WarheadChat plugin;
    public static String pluginName = "[WarheadChat]";
    private static ChatChannels cc;
    private static final Logger log = Logger.getLogger("Minecraft");
    private static WarcraftUtils wu;
    private static ConfigFile cf;
    private static ChatCommand ccom;
    private static ChatCommands ccoms;
    private static ChatFormat help;
    
    @Override
    public void onEnable() {
        log.info(pluginName + " Loading...");
        cc = new ChatChannels();
        wu = WarcraftUtils.getInstance();
        cf = new ConfigFile("plugins/WarheadChat/Channels/","Default");
        help = new ChatFormat();
        
        if(!cf.keyExists("DisplayName")){
            cf.setString("DisplayName", "Def");
        }
        if(!cf.keyExists("Alias")){
            cf.setString("Alias", "D");
        }
        if(!cf.keyExists("OwnerGroup")){
            cf.setString("OwnerGroup", "");
        }
        if(!cf.keyExists("MemberGroup")){
            cf.setString("MemberGroup", "");
        }
        if(!cf.keyExists("Owners")){
            cf.setString("Owners", "");
        }
        if(!cf.keyExists("Members")){
            cf.setString("Members", "@ALL");
        }
        
        cc.playerChannels.clear();
        
        plugin = this;
        this.getEngine().getEventManager().registerEvents(new ChatListener(), this);
        
        //Register commands
        CommandRegistrationsFactory<Class<?>> commandRegFactory = new AnnotatedCommandRegistrationFactory(new SimpleInjector(this), new SimpleAnnotatedCommandExecutorFactory());
        getEngine().getRootCommand().addSubCommands(this, ChatCommand.class, commandRegFactory);

//      this.getEngine().getRootCommand().addSubCommand(Spout.getEngine(), "channels").setHelp("Get all channel commands.").setExecutor(ccom);
        
        log.info(pluginName + "Loaded!");
    }
    
    @Override
    public void onDisable() {
        log.info(pluginName + " Now disabled!");
    }
    /**
     * Gets the instance of WarcraftChat.
     * 
     * @return WarcraftChat
     */
    public static WarheadChat getInstance(){
        return plugin;
    }
    
    /**
     * Gets the instance of ChatChannels.
     * 
     * @return ChatChannels
     */
    public static ChatChannels getChatChannels(){
        return cc;
    }
    
    public static ChatFormat getHelp(){
        return help;
    }
    
    public static List<String> getAllPlayers(){
        List<String> list = Spout.getEngine().getAllPlayers();
        return list;
    }
    
    public void sendHelp(Player player){
        String[] infoSwitch = {"/channel switch <Channel>", "Swiches your current channel to the given one."};
        String[] infoMake = {"/channel make <Channel> [<alias>]", "Creates a new channel and adds an alias automaticly or sets alias to given alias"};
        String[] infoDelete = {"/channel delete <Channel>", "Removes the given channel"};
        String[] infoList = {"/channel list", "Displays all avaliable channels"};
        
        List<String[]> commands = new ArrayList();
        
        commands.add(infoSwitch);
        commands.add(infoMake);
        commands.add(infoDelete);
        commands.add(infoList);
        
        help.setRightColor(ChatStyle.BLUE);
//        help.sendTwoColumnList(player,new ChatArguments().append(ChatStyle.DARK_RED).append("All Channel Commands"), new ChatArguments().append(ChatStyle.DARK_GRAY).append(""), commands);
    }
}
